<?php
/**
 * Created by PhpStorm.
 * User: informatix
 * Date: 1/30/15
 * Time: 9:49 AM
 *
 * convert any number from any base (1-36) to any base (1-36)
 */

error_reporting(E_ERROR);

$number = 5897522333;
$toBase = 32;
$currentBase = 10;


$bases = ["0" => "0" ,"1" => "1" ,"2" => "2" ,"3" => "3" ,"4" => "4" ,"5" => "5" ,"6" => "6" ,"7" => "7" ,"8" => "8" ,"9" => "9" ,"10" => "A", "11" => "B", "12" => "C", "13" => "D", "14" => "E" , "15" => "F" ,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z];

// 0
// 0X
// 0B



/*
 *   number/base until it becomes 0
 *
 */
$prefix = substr($number,0,2);

if(substr($number,0,1) == 0):

    switch($prefix) {

        case "0x":
            $currentBase = 16;
            break;

        case "0b":
            $currentBase = 2;
            break;

        default:
            $currentBase = 8;

    }

endif;

echo "Current Base of number ".$number." is ".$currentBase."<br />";

if($currentBase != 10)
{
    $tmpNumber = "";

    $tmp = (string) $number;

    for($i = strlen($tmp)-1, $j = 0; $i>=0;$i--,$j++) {
        $tmpNumber += intval(array_search($tmp[$i], $bases))*pow($currentBase,$j);
    }

    echo "Number is decimal is $tmpNumber <br />";

    $number = $tmpNumber;
}


$newNumber = "";
do {
    $mod = $number%$toBase;
    $newNumber = $bases[$mod]."".$newNumber;
    $number = (int) ($number/$toBase); // int_val($number/$currentBase);
} while($number > 0);

echo "New number is base $toBase is ".$newNumber;