<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface Shareable
{
public function share();
public function feedback();

}

interface Printer
{
public function printFile();


}


class Twitter implements Shareable, Printer {

    public function share() {
        return "i am twitter share";
    }
    
    public function feedback() {
        return "i am twitter feedback";
    }

     public function printFile() {
         
     }
}

$twitter = new Twitter('twitter_token');
echo $twitter->share();

class Facebook implements Shareable {

    public function __construct($token) {
        $this->token = $token;
    }

    public function share() {
        return "i am facebook share";
    }
    
    public function feedback() {
        return "i am facebook feedback";
    }

}

$facebook = new Facebook('facebook_token');
echo $facebook->share();

