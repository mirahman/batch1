<?php


class Actor {
    
    public $id = "";
    public $dbh = null;
    
    function __construct($id = 0) {
        $this->id = $id;
                
        $this->dbh = new PDO('mysql:host=localhost;dbname=sakila', "root", "");        
    }
    
    
    function getInfo()
    {
        $stmt = $this->dbh->prepare("select * from actor where actor_id = ?");
        $stmt->bindParam(1, $this->id);
        
        $stmt->execute();       
        
        $this->getMovies();
        
        return $stmt->fetch(PDO::FETCH_OBJ);
    }
    
    private function getMovies()
    {
        $stmt = $this->dbh->prepare("select * from film inner join film_actor on film.film_id = film_actor.film_id where actor_id = ?");

        $stmt->bindParam(1, $this->id);
        $stmt->execute(); 
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
        
}


class childActor extends Actor {
    
    public $id = 0;
    
    function __construct($id = 0) {
        
        parent::__construct($id);
        
    }
    
    function getMyId()
    {
        echo "My parent ID is".$this->id;
    }
    
}

class femaleActor extends Actor {
    
    public $id = 0;
    
    function __construct($id = 0) {
        
        parent::__construct($id);       
        
    }
    
    function getMyId()
    {
        echo "My parent ID is".$this->id;
    }
    
}

$actor = new Actor($_GET['id']);

$info = $actor->getInfo();
$movies = $actor->getMovies();





$child = new childActor($_GET['id']);

$child->getMyId();


?>

<body>
    
    First Name:   <?php echo $actor->getInfo()->first_name?> <br />
    Last Name:   <?php echo $info->last_name?> <br />
    
    <h3>Movies done</h3>
    
    <table border="1">
        <tr>
            <td>
                #
            </td>
            <td>Title
            </td>
            <td>Description</td>
            <td>Release Year</td>
            <td>Rating</td>
        </tr>
        
        <?php
         $count = 1;
         foreach($movies as $movie):
        ?>
        
        <tr>
            <td><?php echo $count++?></td>
            <td><?php echo $movie->title?></td>
            <td><?php echo $movie->description?></td>
            <td><?php echo $movie->release_year?></td>
            <td><?php echo $movie->rating?></td>
        </tr>
        
        <?php  endforeach; ?>
        
    </table>
</body>
    
 


