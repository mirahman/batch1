<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class AbstractSocialShare {

    /**
     * Authentication token
     */
    protected $token;

    /**
     * Get token
     * @return string
     */
    public function getToken() {
        return $this->token;
    }

    /**
     * Share
     * @return
      bool
     */
    abstract protected function share();
}

class Twitter extends AbstractSocialShare {

    public function __construct($token) {
        $this->token = $token;
    }

    public function share() {
        return "i am twitter share";
    }

}

$twitter = new Twitter('twitter_token');
echo $twitter->share();

class Facebook extends AbstractSocialShare {

    public function __construct($token) {
        $this->token = $token;
    }

    public function share() {
        return "i am facebook share";
    }

}

$facebook = new Facebook('facebook_token');
echo $facebook->share();

class Pinterest extends AbstractSocialShare {

    public function __construct($token) {
        $this->token = $token;
    }

   public function share() {
        return "i am pinterest share";
    }

}

$pinterest = new Pinterest('facebook_token');
echo $pinterest->share();