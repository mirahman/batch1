<?php
/**
 * Define MyClass
 */
function __autoload($class) {
    require_once("./my_".$class.".php");
}


class MyClass
{
    public $public = 'Public';
    public $protected = 'Protected';
    public $private = 'Private';

    function printHello()
    {
        echo $this->public;
        echo $this->protected;
        echo $this->private;
        
        $actor = new Actor();
    }
}

$obj = new MyClass();
echo $obj->public; // Works
echo $obj->protected; // Fatal Error
echo $obj->private; // Fatal Error
$obj->printHello(); // Shows Public, Protected and Private



