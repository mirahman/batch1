<?php
/**
 * Define MyClass
 */



class MyClass
{
    public $public1 = 'Public';
    //protected $protected = 'Protected';
    //private $private = 'Private';

    final function printHello()
    {
        echo $this->public1;
        //echo $this->protected;
        //echo $this->private;
        
       
    }
    
    public function getProp() {
        return $this->public1;
    }
}

$obj = new MyClass();
echo $obj->public1; // Works
//echo $obj->protected; // Fatal Error
//echo $obj->private; // Fatal Error
$obj->printHello(); // Shows Public, Protected and Private



/**
 * Define MyClass2
 */
class MyClass2 extends MyClass
{
    // We can redeclare the public and protected method, but not private
    protected $protected = 'Protected2';
    public $public1 = "child public";

    protected function printHello1()
    {
        echo $this->public1."<br />";
        echo parent::getProp();
        //echo $this->protected;
        //echo $this->private;
    }
}

class grandChild extends MyClass2 {
    
    function __construct() {
        
        
    }
    
    public function printHello1()
    {
        //echo $this->public1."<br />";
        echo MyClass::getProp();
        //echo $this->protected;
        //echo $this->private;
    }
    
}

$obj2 = new MyClass2();
echo $obj2->public1; // Works
//echo $obj2->protected; // Fatal Error
//echo $obj2->private; // Undefined
$obj2->printHello(); // Shows Public, Protected2, Undefined


