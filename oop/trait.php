<?php
trait Sharable {
  public function share($item= '')
  {
    return 'share this item<br />';
  }
}

trait Security {
  public function secure($item = '')
  {
    return 'secured this item<br />';
  }
}


class Post {
 
  use Sharable, Security;
 
}
 
class Comment {
 
  use Sharable;
 
}

$post = new Post;
echo $post->share(); // 'share this item' 
echo $post->secure(); // 'share this item' 
 
$comment = new Comment;
echo $comment->share(''); // 'share this item'

