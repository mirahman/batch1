<!DOCTYPE html>
<html>
<body>
<?php

/*
Write a switch case statement to decide on student grades. Note: pass mark is 60. up to 70 is D, 
     up to 80 is C, up to 90 is B and up to 100 is A.
*/

 echo "<br>"; 
 $passmark=75; 
 
 if($passmark>0 && $passmark<=100):
 switch ($passmark):
    case ($passmark==60):
        echo "Pass Grage and Marks:".$passmark;
        break;
    case ($passmark>60 && $passmark<=70):
        echo "D Grade and Marks:".$passmark;
        break;
    case ($passmark>70 && $passmark<=80):
        echo "C Grade and Marks:".$passmark;
        break;
    case ($passmark>80 && $passmark<=90):
        echo "B Grade and Marks:".$passmark;
        break;
    case ($passmark>90 && $passmark<=100):
        echo "A Grade and Marks:".$passmark;
        break;
    default :
       echo "Fail mark:".$passmark;         
 endswitch;
 else:
     echo "Invalid Grade number";  
 endif;   
 
 echo "<hr>";
 
 
 /*Write a program to run from 1 to 20,000 and only prints numbers which are only divided by 7. */
 for($i=1;$i<=2000;$i++): 
    if($i%7==0):
      echo $i,", ";   
    endif;
  endfor;
  
 echo "<hr>";
 
 
/*Write a program to print the following drawing
X
XX
XXX
XXXX
XXX
XX
X

Note: This is partly done as I could not decrement.
*/

for($i=1;$i<=4;$i++):
  for($j=0;$j<$i;$j++):
    echo "X";   
  endfor;
  echo "<br>";
endfor;

echo "<hr>";
 
 ?>
 
 </body>
</html>