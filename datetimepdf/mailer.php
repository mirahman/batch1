<?php
session_start();
if(! in_array($_POST['secretKey'], $_SESSION['FORMKEY' ])){
    die("sorry you are not authorized to access this form");
}
if($_FILES['file']) {
    move_uploaded_file($_FILES['file']['tmp_name'],"./images/".$_FILES['file']['name']);
}

$str = "";

foreach($_POST as $val) {
    $str .= $val."\n";
}

require 'phpmailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

/*
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp1.example.com;smtp2.example.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'user@example.com';                 // SMTP username
$mail->Password = 'secret';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to
*/

$mail->From = 'info@techmasters.co';
$mail->FromName = 'TechMasters';
$mail->addAddress('raazxplorer@gmail.com', 'Raz bhai');     // Add a recipient
//$mail->addAddress('ellen@example.com');               // Name is optional
$mail->addReplyTo('info@techmasters.co', 'Information');
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

$mail->addAttachment('./images/'.$_FILES['file']['name']);         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$str .= $_SERVER['SERVER_NAME']."/images/".$_FILES['file']['name'];

$mail->Subject = 'attachment test';
$mail->Body    = nl2br($str);
$mail->AltBody = $str;

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}

